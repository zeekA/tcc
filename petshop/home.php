<html lang="pt-br">
<head>
     <meta charset="utf-8"/>
     <title> PetShop</title>
	 
	 
	 <link rel="stylesheet" href="css/animate.css">    
    <link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="CSS/estilo9.css">
	
</head>
<body>	 
    
  <div id = "menu">
     <ul>
	<li><a href = "home.php"><strong>Inicio</strong></a></li>
    <li><a href = "cadastrar.php">Cadastre-se</a></li>
	<li><a href = "index.php">Entrar</a></li>
    
	</ul>  
  </div> 
	
	
    </div>
  </nav>

    
    <div class="hero-wrap js-fullheight" style="background-image: url('images/dog.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-9 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
            <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 0.1 }"><strong>Encontre <br></strong> seu Petshop preferido da cidade</h1>
            <p data-scrollax="properties: { translateY: '30%', opacity: 0.1 }">encontre otimos petshops para levar seu cachorro, fazer compras e conversar com especialistas</p>
           
              
                  
                  
            
            </p>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section services-section bg-light">
      <div class="container">
        <div class="row d-flex">
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-guarantee"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Melhores serviços da região</h3>
                <p>
						Temos as melhores lojas
							disponiveis no nosso site.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-like"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Cuidamos do seu animal de estimação</h3>
                <p>Nas lojas contamos
						com o melhor atendimento
							para seu animal.</p>
              </div>
            </div>    
          </div>
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-detective"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Serviço 100% garantido</h3>
                <p> 
					Serviço com profissionais
						de qualidade, pronto
							para atender necessidades.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-support"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Profissionais de qualidade</h3>
                <p> 
							Temos os melhores funcionarios
								disponiveis, para cuidar
									do seu animal de estimação.</p>
              </div>
            </div>      
          </div>
        </div>
      </div>
    </section>
    
    

            <p>

  </p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>
	
    
    
 
	 